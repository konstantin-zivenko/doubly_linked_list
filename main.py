# Взявши за основу код прикладу example_5.py, розширте функціональність класу MyList,
# додавши методи:
# - очищення списку - clear
# - додавання елемента у довільне місце списку - insert,
# - видалення елемента з кінця - pop
# - видалення елемента з довільного місця списку pop.

# Перед початком вивчіть - що таке двобічно звʼязаний список
# https://uk.wikipedia.org/wiki/%D0%94%D0%B2%D0%BE%D0%B1%D1%96%D1%87%D0%BD%D0%BE_%D0%B7%D0%B2%27%D1%8F%D0%B7%D0%B0%D0%BD%D0%B8%D0%B9_%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA

# Курс на тему структур даних у python (українською)
# https://prometheus.org.ua/course/course-v1:Michigan+PDS101+2023_T3


class MyList(object):
    """Класс списка"""

    class _ListNode(object):
        """Внутрішній клас екземплярів списку."""

        # За умовчанням атрибути-дані зберігаються у словнику __dict__.
        # Якщо можливість динамічно додавати нові атрибути
        # не потрібно, можна заздалегідь їх описати, що більше
        # ефективно з точки зору пам'яті та швидкодії, що особливо
        # важливо, коли створюється безліч екземлярів даного класу.
        # https://docs.python.org/3/reference/datamodel.html#slots
        __slots__ = ('value', 'prev', 'next')

        def __init__(self, value, prev=None, next=None):
            self.value = value
            self.prev = prev
            self.next = next

        def __repr__(self):
            return f'MyList._ListNode({self.value}, {id(self.prev)}, {id(self.next)})'

    class _Iterator(object):
        """Внутрішні клас ітератору."""

        def __init__(self, list_instance):
            self._list_instance = list_instance
            self._next_node = list_instance._head

        def __iter__(self):
            return self

        def __next__(self):
            if self._next_node is None:
                raise StopIteration

            value = self._next_node.value
            self._next_node = self._next_node.next

            return value

    def __init__(self, iterable=None):
        # Довжина списку
        self._length = 0
        # Перший елемент списку
        self._head = None
        # Останній елемент списку
        self._tail = None

        # Додавання всіх переданих елементів
        if iterable is not None:
            for element in iterable:
                self.append(element)

    def append(self, element):
        """Додавання елемента в кінець списку."""

        # Створення елементу списку.
        node = MyList._ListNode(element)

        if self._tail is None:
            # Список все ще пустий.
            self._head = self._tail = node
        else:
            # Додавання елемента.
            self._tail.next = node
            node.prev = self._tail
            self._tail = node

        self._length += 1

    def __len__(self):
        return self._length

    def __repr__(self):
        # Метод join класу str приймає послідовність рядків
        # і повертає рядок, у якому всі елементи цієї
        # Послідовності з'єднані початковим рядком.
        # Функція map застосовує задану функцію всім елементам послідовності.
        return f'MyList([{', '.join(map(repr, self))}])'

    def __getitem__(self, index):
        # https://docs.python.org/3/reference/datamodel.html#object.__getitem__
        if not 0 <= index < len(self):
            raise IndexError('list index out of range')

        node = self._head
        for _ in range(index):
            node = node.next

        return node.value

    def __iter__(self):
        return MyList._Iterator(self)


def main():
    # Створення списку
    my_list = MyList([1, 2, 5])

    # Вивід довжини списку
    print(len(my_list))

    # Вивід самого списку
    print(my_list)

    print()

    # Ітерація по списку
    for element in my_list:
        print(element)

    print()

    # Повторна ітерація по списку
    for element in my_list:
        print(element)


if __name__ == '__main__':
    main()
